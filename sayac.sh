#!/usr/bin/bash

i=-1
sure=$1
mesaj=$2
re='^[0-9]+$'

if [ "$sure" == "" ]
then
notify-send -u critical --expire-time=10000 "Eksik süre değeri!" "Çıkış yapıldı."
exit
fi

if ! [[ "$sure" =~ $re ]]
then
notify-send -u critical --expire-time=10000 "Geçersiz süre değeri!" "Çıkış yapıldı."
exit
fi

if [ "$mesaj" == "" ]
then
notify-send -i task-due --expire-time=500 "Uyarı: " "Mesaj metni girilmedi!"
fi

while [ $i -lt $sure ]
do
i=$(($i + 1))
sonuc=$(($sure-$i))
echo "$sonuc saniye kaldı."
sleep 1
done

if [ "$mesaj" == "" ]
then
bildiri="$sure saniye sona erdi!"
notify-send -i user-available -u critical -t 0 "$bildiri"
exit
fi

if ! [ "$mesaj" == "" ]
then
bildiri="$sure saniye sona erdi!"
notify-send -i user-available -u critical -t 0 "$bildiri Mesajınız: " "$mesaj"
exit
fi
